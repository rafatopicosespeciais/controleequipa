import { Injectable } from '@angular/core';
import { FakeDataService } from './fake-data.service';
import { Equipamentos } from '../interface/equipamentos';

@Injectable({
  providedIn: 'root'
})
export class EquipamentosService {
  public equipamentosList: Equipamentos[];

  constructor(
    // acesso ao banco de dados
    private data: FakeDataService
  ) {
    this.equipamentosList = data.get();
   }
   save(equipamentos: Equipamentos[]){
     this.data.set(equipamentos);
   }
}
