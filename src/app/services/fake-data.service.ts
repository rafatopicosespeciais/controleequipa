import { Injectable } from '@angular/core';
import { Equipamentos } from '../interface/equipamentos';

@Injectable({
  providedIn: 'root'
})
export class FakeDataService {
  
  public get(){
    const aux = localStorage.getItem('data');
    if(aux){
      return JSON.parse(aux);
    }
    return [];
  }

  set(equip: Equipamentos[]){
    const aux = JSON.stringify(equip);
    localStorage.setItem('data', aux);
  }
}