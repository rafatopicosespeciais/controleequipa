import { Component, OnInit } from '@angular/core';
import { Equipamentos } from 'src/app/interface/equipamentos';
import { AlertController } from '@ionic/angular';
import { EquipamentosService } from 'src/app/services/equipamentos.service';



@Component({
  selector: 'app-reserva',
  templateUrl: './reserva.page.html',
  styleUrls: ['./reserva.page.scss'],
})
export class ReservaPage implements OnInit {
  lista: Equipamentos[];

  constructor(
    private service: EquipamentosService,
    private alert: AlertController
  ) { }

  ngOnInit() {
    this.lista = this.service.equipamentosList;

  }

  async newEquipamento(){
    const form = await this.alert.create({
      header: 'Equipamento',
      message: 'Digite o nome da equipamento',
     inputs: [
        {type: 'text', name: 'equipamento'},
        {type: 'text', name: 'foto'}
      ],
      buttons: [
        {text: 'Cancelar'},
        {text: 'Salvar', handler: data => this.save(data)}
      ]
    });

    form.present();
  }

  private save(data){
    const idt = this.lista.length + 1;
    const equipamentos: Equipamentos = {id: this.lista.length + 1, title: data.equipamento, fotos: data.foto, 
      situacao: false};
    this.lista.push(equipamentos);
    this.service.save(this.lista);
  }

  async newReserva(equip: Equipamentos){
    const form = await this.alert.create({
      header: 'Desejar fazer a reserva?'  ,
      buttons: [
        {text: 'Não'},
        {text: 'Sim', handler: data => this.edita(equip, data)}
      ]
    });

    form.present();
  }

  private edita(equip: Equipamentos, data){
    equip.situacao = true;
    this.service.save(this.lista);
  }
}
