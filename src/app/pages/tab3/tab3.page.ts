import { Component } from '@angular/core';
import { Equipamentos } from 'src/app/interface/equipamentos';
import { EquipamentosService } from 'src/app/services/equipamentos.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  lista: Equipamentos[];

  constructor(
    private service: EquipamentosService,
    private alert: AlertController
  ) {}

  ngOnInit() {
    this.lista = this.service.equipamentosList;
  }

  private save(data){
    const idt = this.lista.length + 1;
    const equipamentos: Equipamentos = {id: this.lista.length + 1, title: data.equipamento, fotos: data.foto, 
      situacao: false};
    this.lista.push(equipamentos);
    this.service.save(this.lista);
  }

  async newReserva(equip: Equipamentos){
    const form = await this.alert.create({
      header: 'Desejar devolver?',
      buttons: [
        {text: 'Não'},
        {text: 'Sim', handler: data => this.edita(equip, data)}
      ]
    });

    form.present();
  }

  private edita(equip: Equipamentos, data){
    equip.situacao = false;
    this.service.save(this.lista);
  }

}
