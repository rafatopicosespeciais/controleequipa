import { Component } from '@angular/core';
import { Equipamentos } from 'src/app/interface/equipamentos';
import { AlertController } from '@ionic/angular';
import { EquipamentosService } from 'src/app/services/equipamentos.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  lista: Equipamentos[];

  constructor(
    private service: EquipamentosService,
    private alert: AlertController
  ) {}

  ngOnInit() {
    this.lista = this.service.equipamentosList;
  }

}


